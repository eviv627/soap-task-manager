package ru.evseenko.api.endpoint;

import ru.evseenko.entity.dto.ProjectDTO;

import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;

@WebService
@XmlSeeAlso({ProjectDTO.class})
public interface IProjectEndpoint extends IEntityEndpoint<ProjectDTO> {
}
