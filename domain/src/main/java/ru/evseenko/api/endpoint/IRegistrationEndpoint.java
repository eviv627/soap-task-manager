package ru.evseenko.api.endpoint;

import ru.evseenko.entity.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IRegistrationEndpoint {
    @WebMethod
    UserDTO register(@WebParam(name = "user") UserDTO userDTO);

    @WebMethod
    UserDTO remove(@WebParam(name = "user") UserDTO userDTO);
}
