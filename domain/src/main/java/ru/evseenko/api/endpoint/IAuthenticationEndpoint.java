package ru.evseenko.api.endpoint;

import ru.evseenko.entity.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAuthenticationEndpoint {
    @WebMethod
    UserDTO authenticate(@WebParam(name = "login") String login, @WebParam(name = "password") String password);
}
