package ru.evseenko.api.endpoint;

import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IEntityEndpoint<T> {
    @WebMethod
    @NotNull
    T get(
            @WebParam(name = "id") @NotNull String id
    );

    @WebMethod
    @NotNull
    List<T> getAll();

    @WebMethod
    @NotNull
    List<T> findByName(
            @WebParam(name = "name") @NotNull String name
    );

    @WebMethod
    @NotNull
    List<T> findByDescription(
            @WebParam(name = "description") @NotNull String description
    );

    @WebMethod
    void update(
            @WebParam(name = "entity") @NotNull T entity
    );

    @WebMethod
    T persist(
            @WebParam(name = "entity") @NotNull T entity
    );

    @WebMethod
    void delete(
            @WebParam(name = "entity") @NotNull T entity
    );

    @WebMethod
    void deleteAll();
}