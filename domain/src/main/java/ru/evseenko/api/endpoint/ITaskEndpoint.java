package ru.evseenko.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.entity.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

@WebService
@XmlSeeAlso({TaskDTO.class})
public interface ITaskEndpoint extends IEntityEndpoint<TaskDTO> {
    @WebMethod
    List<TaskDTO> getTaskForProject(
            @WebParam(name = "project") @NotNull String projectId
    );
}
