package ru.evseenko.util;

public class ConstantApplication {
    public static final String PROJECT_ENDPOINT_URL = "http://localhost:8080/services/project/";
    public static final String TASK_ENDPOINT_URL = "http://localhost:8080/services/task/";
    public static final String AUTHENTICATION_ENDPOINT_URL = "http://localhost:8080/services/authentication/";
    public static final String REGISTRATION_ENDPOINT_URL = "http://localhost:8080/services/registration/";
}
