package ru.evseenko.util;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.entity.DomainDTO;
import ru.evseenko.api.entity.SortableEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public enum Sort { START_DATE("start date"), END_DATE("end date"), CREATE("create"), READY("status"), NAME("name");
    final private String name;

    Sort(String name) {
        this.name = name;
    }

    public static void sortEntities(@NotNull List<? extends SortableEntity> entities, @NotNull Sort sort) {
        entities.sort(sort.getComparator());
    }

    public static void sortDTO(@NotNull List<? extends DomainDTO> entities, @NotNull Sort sort) {
        entities.sort(sort.getComparatorDTO());
    }

    @NotNull
    public String getName() {
        return name;
    }

    @NotNull
    public static Sort getByName(@NotNull String name) {
        Sort sort = null;

        for (Sort s : values()) {
            if(s.getName().equalsIgnoreCase(name)) {
                sort = s;
            }
        }

        if (sort == null) {
            throw new IllegalArgumentException("no sort by : " + name);
        }
        return sort;
    }

    @NotNull
    public Comparator<SortableEntity> getComparator() {

        if (this == START_DATE) {
            return Comparator.comparing(SortableEntity::getStartDate);
        }

        if (this == END_DATE) {
            return Comparator.comparing(SortableEntity::getEndDate);
        }

        if (this == CREATE) {
            return Comparator.comparing(SortableEntity::getCreateDate);
        }

        if (this == READY) {
            return (o1, o2) -> {
                final int num1 = Objects.requireNonNull(o1.getStatus()).getNum();
                final int num2 = Objects.requireNonNull(o2.getStatus()).getNum();
                if (num1 != num2) {
                    return num1 - num2;
                }
                return Objects.requireNonNull(o1.getName()).compareTo(Objects.requireNonNull(o2.getName()));
            };
        }

        if (this == NAME) {
            return Comparator.comparing(SortableEntity::getName);
        }

        throw new IllegalArgumentException("no such comparator");
    }

    @NotNull
    public Comparator<DomainDTO> getComparatorDTO() {

        if (this == START_DATE) {
            return Comparator.comparing(DomainDTO::getStartDate);
        }

        if (this == END_DATE) {
            return Comparator.comparing(DomainDTO::getEndDate);
        }

        if (this == CREATE) {
            return Comparator.comparing(DomainDTO::getCreateDate);
        }

        if (this == READY) {
            return (o1, o2) -> {
                final int num1 = Objects.requireNonNull(o1.getStatus()).getNum();
                final int num2 = Objects.requireNonNull(o2.getStatus()).getNum();
                if (num1 != num2) {
                    return num1 - num2;
                }
                return Objects.requireNonNull(o1.getName()).compareTo(Objects.requireNonNull(o2.getName()));
            };
        }

        if (this == NAME) {
            return Comparator.comparing(DomainDTO::getName);
        }

        throw new IllegalArgumentException("no such comparator");
    }
}
