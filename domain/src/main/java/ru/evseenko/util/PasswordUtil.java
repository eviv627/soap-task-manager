package ru.evseenko.util;

import java.util.Base64;

public class PasswordUtil {

    public static String encode(final String string) {
        return Base64.getEncoder().encodeToString(string.getBytes());
    }

    public static String decode(final String string) {
        return new String(Base64.getDecoder().decode(string));
    }
}
