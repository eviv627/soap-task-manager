package ru.evseenko.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.evseenko.api.repository.IProjectRepository;
import ru.evseenko.api.repository.IUserRepository;
import ru.evseenko.api.service.IProjectService;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.User;
import ru.evseenko.entity.dto.ProjectDTO;
import ru.evseenko.exception.ServiceException;

import java.util.Optional;

@Service
public class ProjectService extends AbstractDomainService<ProjectDTO, Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    private final IUserRepository userRepository;

    @Autowired
    public ProjectService(IProjectRepository projectRepository, IUserRepository userRepository) {
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
    }

    @NotNull
    @Override
    IProjectRepository getDomainRepository() {
        return projectRepository;
    }

    @Override
    void updateEntity(
            @NotNull final Project entity,
            @NotNull final ProjectDTO entityDTO
    ) {
        @NotNull final Optional<User> optional = userRepository.findById(entityDTO.getUserId());
        if(!optional.isPresent()) {
            throw new SecurityException("no such user");
        }
        entity.setUser(optional.get());
    }

    @Override
    void updateDTO(
            @NotNull final Project entity,
            @NotNull final ProjectDTO entityDTO
    ) {
        if(entity.getUser() == null) {
            throw new ServiceException("Project is not valid");
        }
        @NotNull final User user = entity.getUser();
        if(user.getId() == null) {
            throw new ServiceException("User is not valid");
        }
        entityDTO.setUserId(user.getId());
    }

    @Override
    @NotNull
    Class<Project> getClassEntity() {
        return Project.class;
    }

    @Override
    @NotNull
    Class<ProjectDTO> getClassDTO() {
        return ProjectDTO.class;
    }
}
