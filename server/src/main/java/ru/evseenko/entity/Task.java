package ru.evseenko.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import org.springframework.cache.annotation.Cacheable;

import javax.persistence.*;


@Entity
@Setter
@Getter
@Cacheable
@NoArgsConstructor
@Table(name = "app_task")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
        @NamedQuery(
                name = Task.FIND_ALL,
                query = "select task from Task task where task.user.id = :userId",
                hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }

        ),
        @NamedQuery(
                name = Task.FIND_BY_NAME,
                query = "select task from Task task where task.name like CONCAT(:name,'%') " +
                "and task.user.id = :userId",
                hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }
        ),
        @NamedQuery(
                name = Task.FIND_BY_DESC,
                query = "select task from Task task where task.description like CONCAT(:description,'%') " +
                "and task.user.id = :userId",
                hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }
        ),
        @NamedQuery(
                name = Task.FIND_BY_PROJECT,
                query = "select task from Task task where task.project.id = :projectId " +
                "and task.user.id = :userId",
                hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }
        )
})
public class Task extends Domain {

    public static final String FIND_ALL = "Task.findAll";
    public static final String FIND_BY_NAME = "Task.findByNamePart";
    public static final String FIND_BY_DESC = "Task.findByDescriptionPart";
    public static final String FIND_BY_PROJECT = "Task.findTaskForProject";
    public static final String DELETE_ALL = "Task.deleteAll";

    @Nullable
    @JoinColumn(name = "project_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Project project;
}
