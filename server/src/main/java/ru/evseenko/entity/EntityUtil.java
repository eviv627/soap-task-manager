package ru.evseenko.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EntityUtil {
    private static final String DATE_FORMAT_PATTERN = "dd.MM.yyyy";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_PATTERN);

    public static String printDate(Date date) {
        return date == null ? "null" : DATE_FORMAT.format(date);
    }
}
