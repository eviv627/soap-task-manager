package ru.evseenko.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import org.springframework.cache.annotation.Cacheable;

import javax.persistence.*;
import java.util.List;


@Entity
@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Table(name = "app_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
        @NamedQuery(
                name = User.FIND_BY_LOGIN,
                query = "SELECT user from User user where user.login = :login",
                hints = { @QueryHint(name = "org.hibernate.cacheable", value = "true") }
        )
})
public class User extends Identifiable {

    public static final String FIND_BY_LOGIN = "User.findUserByLogin";

    @Nullable
    @Column(name="login", unique = true)
    private String login;

    @Nullable
    @Column(name="password_hash", nullable = false)
    private String passwordHash;

    @Nullable
    @Column(name="role")
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(name = "active", nullable = false)
    private boolean active;

    @Nullable
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projects;

    @Nullable
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks;
}
