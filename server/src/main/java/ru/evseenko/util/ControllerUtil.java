package ru.evseenko.util;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.evseenko.entity.dto.UserDTO;

public class ControllerUtil {
    @NotNull
    public static UserDTO getCurrentUser() {
        return (UserDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    @NotNull
    public static String getCurrentUserId() {
        @NotNull final UserDTO user = getCurrentUser();
        return user.getId();
    }
}
