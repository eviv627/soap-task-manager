package ru.evseenko.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.evseenko.entity.Task;

import java.util.List;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String>, IDomainRepository<Task> {

    @NotNull List<Task> findAll(@NotNull @Param("userId") String userId);

    @NotNull List<Task> findByNamePart(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @NotNull List<Task> findByDescriptionPart(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("description") String description
    );

    @NotNull List<Task> findTaskForProject(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId
    );
}
