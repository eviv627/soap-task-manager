package ru.evseenko.api.service;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.entity.DomainDTO;

import java.util.List;

public interface IDomainService<T extends DomainDTO> {

    @NotNull
    T get(@NotNull String userId, @NotNull String id);

    @NotNull
    List<T> getAll(@NotNull String userId);

    @NotNull
    List<T> findByNamePart(@NotNull String userId, @NotNull String name);

    @NotNull
    List<T> findByDescriptionPart(@NotNull String userId, @NotNull String description);

    void update(@NotNull String userId, @NotNull T entityDTO);

    void persist(@NotNull String userId, @NotNull T entityDTO);

    void delete(@NotNull String userId, @NotNull T entityDTO);

    void deleteAll(@NotNull String userId);
}
