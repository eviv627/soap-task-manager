package ru.evseenko.api.service;

import ru.evseenko.entity.dto.ProjectDTO;

public interface IProjectService extends IDomainService<ProjectDTO> {
}
