package ru.evseenko.api.service;

import org.jetbrains.annotations.NotNull;
import ru.evseenko.entity.dto.TaskDTO;

import java.util.List;

public interface ITaskService extends IDomainService<TaskDTO> {
    @NotNull
    List<TaskDTO> getTaskForProject(@NotNull String userId, @NotNull final String projectId);
}
