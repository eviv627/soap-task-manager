package ru.evseenko.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.evseenko.api.endpoint.IRegistrationEndpoint;
import ru.evseenko.api.service.IUserService;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.dto.UserDTO;
import ru.evseenko.exception.ServiceException;
import ru.evseenko.util.PasswordUtil;

import javax.jws.WebService;
import java.util.UUID;

@Component
@WebService(endpointInterface = "ru.evseenko.api.endpoint.IRegistrationEndpoint")
public class RegistrationEndpoint implements IRegistrationEndpoint {

    private final IUserService userService;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationEndpoint(IUserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDTO register(final UserDTO userDTO) {
        if (userService.getUserByLogin(userDTO.getLogin()) != null) {
            throw new ServiceException("Already exist");
        }

        if (userDTO.getLogin().isEmpty()) {
            throw new ServiceException("Incorrect login");
        }

        if (userDTO.getPasswordHash().isEmpty()) {
            throw new ServiceException("Incorrect password");
        }

        userDTO.setId(UUID.randomUUID().toString());
        userDTO.setPasswordHash(passwordEncoder.encode(userDTO.getPasswordHash()));
        userDTO.setActive(true);
        userDTO.setRole(Role.ADMIN);
        userService.persist(userDTO);

        return userDTO;
    }

    @Override
    public UserDTO remove(final UserDTO userDTO) {
        userService.remove(userDTO.getId());
        return userDTO;
    }
}
