package ru.evseenko.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.evseenko.api.endpoint.ITaskEndpoint;
import ru.evseenko.api.service.ITaskService;
import ru.evseenko.entity.dto.TaskDTO;
import ru.evseenko.util.ControllerUtil;

import javax.jws.WebService;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component
@WebService(endpointInterface = "ru.evseenko.api.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {
    
    private final ITaskService taskService;

    @Autowired
    public TaskEndpoint(ITaskService taskService) {
        this.taskService = taskService;
    }

    @NotNull
    @Override
    public TaskDTO get(@NotNull String id) {
        return taskService.get(ControllerUtil.getCurrentUserId(), id);
    }

    @Override
    public @NotNull List<TaskDTO> getAll() {
        return taskService.getAll(ControllerUtil.getCurrentUserId());
    }

    @Override
    public @NotNull List<TaskDTO> findByName(@NotNull String name) {
        return taskService.findByNamePart(ControllerUtil.getCurrentUserId(), name);
    }

    @Override
    public @NotNull List<TaskDTO> findByDescription(@NotNull String description) {
        return taskService.findByDescriptionPart(ControllerUtil.getCurrentUserId(), description);
    }

    @Override
    public void update(@NotNull TaskDTO entity) {
        taskService.update(ControllerUtil.getCurrentUserId(), entity);
    }

    @Override
    public TaskDTO persist(@NotNull final TaskDTO entity) {
        entity.setId(UUID.randomUUID().toString());
        entity.setCreateDate(new Date());
        entity.setUserId(ControllerUtil.getCurrentUserId());
        taskService.persist(ControllerUtil.getCurrentUserId(), entity);
        return entity;
    }

    @Override
    public void delete(@NotNull TaskDTO entity) {
        taskService.delete(ControllerUtil.getCurrentUserId(), entity);
    }

    @Override
    public void deleteAll() {
        taskService.deleteAll(ControllerUtil.getCurrentUserId());
    }

    @Override
    public List<TaskDTO> getTaskForProject(@NotNull String projectId) {
        return taskService.getTaskForProject(ControllerUtil.getCurrentUserId(), projectId);
    }
}
