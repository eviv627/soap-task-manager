package ru.evseenko.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.evseenko.api.endpoint.IAuthenticationEndpoint;
import ru.evseenko.api.service.IUserService;
import ru.evseenko.entity.dto.UserDTO;
import ru.evseenko.exception.ServiceException;

import javax.jws.WebService;
import java.security.Security;

@Component
@WebService(endpointInterface = "ru.evseenko.api.endpoint.IAuthenticationEndpoint")
public class AuthenticationEndpoint implements IAuthenticationEndpoint {

    private final AuthenticationManager authenticationManager;

    private final IUserService userService;

    @Autowired
    public AuthenticationEndpoint(AuthenticationManager authenticationManager, IUserService userService) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
    }

    @Override
    public UserDTO authenticate(String login, String password) {

        @Nullable final UserDTO user = userService.getUserByLogin(login);

        if (user == null) {
            throw new ServiceException("No Such User");
        }

        final Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login, password));
        if (authenticate.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(authenticate);
        }

        return user;
    }
}
