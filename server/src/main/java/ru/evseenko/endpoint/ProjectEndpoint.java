package ru.evseenko.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.evseenko.api.endpoint.IProjectEndpoint;
import ru.evseenko.api.service.IProjectService;
import ru.evseenko.entity.dto.ProjectDTO;
import ru.evseenko.util.ControllerUtil;

import javax.jws.WebService;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component
@WebService(endpointInterface = "ru.evseenko.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    private final IProjectService projectService;

    @Autowired
    public ProjectEndpoint(IProjectService projectService) {
        this.projectService = projectService;
    }

    @NotNull
    @Override
    public ProjectDTO get(@NotNull String id) {
        return projectService.get(ControllerUtil.getCurrentUserId(), id);
    }

    @Override
    public @NotNull List<ProjectDTO> getAll() {
        return projectService.getAll(ControllerUtil.getCurrentUserId());
    }

    @Override
    public @NotNull List<ProjectDTO> findByName(@NotNull String name) {
        return projectService.findByNamePart(ControllerUtil.getCurrentUserId(), name);
    }

    @Override
    public @NotNull List<ProjectDTO> findByDescription(@NotNull String description) {
        return projectService.findByDescriptionPart(ControllerUtil.getCurrentUserId(), description);
    }

    @Override
    public void update(@NotNull ProjectDTO entity) {
        projectService.update(ControllerUtil.getCurrentUserId(), entity);
    }

    @Override
    public ProjectDTO persist(@NotNull ProjectDTO entity) {
        entity.setId(UUID.randomUUID().toString());
        entity.setCreateDate(new Date());
        entity.setUserId(ControllerUtil.getCurrentUserId());
        projectService.persist(ControllerUtil.getCurrentUserId(), entity);
        return  entity;
    }

    @Override
    public void delete(@NotNull ProjectDTO entity) {
        projectService.delete(ControllerUtil.getCurrentUserId(), entity);
    }

    @Override
    public void deleteAll() {
        projectService.deleteAll(ControllerUtil.getCurrentUserId());
    }
}
