package ru.evseenko.config;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.evseenko.api.endpoint.IAuthenticationEndpoint;
import ru.evseenko.api.endpoint.IProjectEndpoint;
import ru.evseenko.api.endpoint.IRegistrationEndpoint;
import ru.evseenko.api.endpoint.ITaskEndpoint;

import javax.xml.ws.Endpoint;

@Configuration
@ComponentScan(basePackages = {
        "ru.evseenko.endpoint",
        "ru.evseenko.service"
})
public class SoapConfig {
    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }

    @Bean
    public Endpoint endpointProject(final IProjectEndpoint projectEndpoint) {
        EndpointImpl endpoint = new EndpointImpl(springBus(), projectEndpoint);
        endpoint.publish("/project");
        return endpoint;
    }

    @Bean
    public Endpoint endpointTask(final ITaskEndpoint taskEndpoint) {
        EndpointImpl endpoint = new EndpointImpl(springBus(), taskEndpoint);
        endpoint.publish("/task");
        return endpoint;
    }

    @Bean
    public Endpoint endpointAuthentication(final IAuthenticationEndpoint authenticationEndpoint) {
        EndpointImpl endpoint = new EndpointImpl(springBus(), authenticationEndpoint);
        endpoint.publish("/authentication");
        return endpoint;
    }

    @Bean
    public Endpoint endpointRegistration(final IRegistrationEndpoint registrationEndpoint) {
        EndpointImpl endpoint = new EndpointImpl(springBus(), registrationEndpoint);
        endpoint.publish("/registration");
        return endpoint;
    }
}
