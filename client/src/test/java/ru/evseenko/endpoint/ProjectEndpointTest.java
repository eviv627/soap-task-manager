package ru.evseenko.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.evseenko.api.endpoint.IAuthenticationEndpoint;
import ru.evseenko.api.endpoint.IProjectEndpoint;
import ru.evseenko.api.endpoint.IRegistrationEndpoint;
import ru.evseenko.api.endpoint.ITaskEndpoint;
import ru.evseenko.entity.dto.ProjectDTO;
import ru.evseenko.entity.dto.TaskDTO;
import ru.evseenko.entity.dto.UserDTO;
import ru.evseenko.util.ConstantApplication;
import ru.evseenko.util.Sort;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public class ProjectEndpointTest {

    private IProjectEndpoint projectService;

    private ITaskEndpoint taskService;

    private IAuthenticationEndpoint authenticationService;

    private IRegistrationEndpoint registrationService;

    private UserDTO testUserDTO;

    @Before
    public void init() {
        initializeAuthenticationService();
        initializeRegistrationService();
        initializeProjectService();
        initializeTaskService();

        final UserDTO testUser = new UserDTO();
        testUser.setLogin(generateUserName());
        testUser.setPasswordHash("12345678");
        testUserDTO = registrationService.register(testUser);
        testUserDTO.setPasswordHash("12345678");

        authenticate();
    }

    @After
    public void tearDown() {
        registrationService.remove(testUserDTO);
    }

    @Test
    public void projectCreateTest() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("project1");
        project.setDescription("desc");
        project.setStartDate(new Date());
        project.setEndDate(new Date());

        @NotNull final ProjectDTO projectResponse = projectService.persist(project);

        project.setId(projectResponse.getId());
        project.setUserId(projectResponse.getUserId());
        project.setCreateDate(projectResponse.getCreateDate());

        @NotNull final ProjectDTO actualProject =
                projectService.get(project.getId());

        Assert.assertEquals(project.toString(), actualProject.toString());
    }

    @Test
    public void projectUpdateTest() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("project1");
        project.setDescription("desc");
        project.setStartDate(new Date());
        project.setEndDate(new Date());

        @NotNull final ProjectDTO projectResponse = projectService.persist(project);

        project.setId(projectResponse.getId());
        project.setUserId(projectResponse.getUserId());
        project.setCreateDate(projectResponse.getCreateDate());

        project.setName("upd_project1");
        project.setDescription("upd_desc");
        project.setStartDate(new Date());
        project.setEndDate(new Date());

        projectService.update(project);

        @NotNull final ProjectDTO actualProject =
                projectService.get(project.getId());

        Assert.assertEquals(project.toString(), actualProject.toString());
    }

    @Test
    public void projectDeleteAllTest() {
        final int projectsNum = 20;
        final int tasksNum = 20;

        @NotNull final List<ProjectDTO> listProjectDTO = new ArrayList<>();

        for (int i = 0; i < projectsNum; i++) {
            ProjectDTO project = new ProjectDTO();
            project.setId(UUID.randomUUID().toString());
            project.setName("project" + i);
            project.setUserId(testUserDTO.getId());
            project.setDescription("desc");
            project.setCreateDate(new Date());

            project = projectService.persist(project);
            listProjectDTO.add(project);
        }

        for (int i = 0; i < tasksNum; i++) {
            final TaskDTO task = new TaskDTO();
            task.setId(UUID.randomUUID().toString());
            task.setName("project" + i);
            task.setDescription("desc");
            task.setProjectId(listProjectDTO.get(i).getId());
            taskService.persist(task);
        }

        projectService.deleteAll();

        Assert.assertEquals(new ArrayList<>(), taskService.getAll());
        Assert.assertEquals(new ArrayList<>(), projectService.getAll());
    }

    @Test
    public void projectDeleteTaskTest() {
        final int projectsNum = 20;
        final int tasksNum = 200;

        @NotNull final List<ProjectDTO> listProjectDTO = new ArrayList<>();
        @NotNull final List<TaskDTO> listTaskDTO = new ArrayList<>();

        for (int i = 0; i < projectsNum; i++) {
            ProjectDTO project = new ProjectDTO();
            project.setId(UUID.randomUUID().toString());
            project.setName("project" + i);
            project.setDescription("desc");
            project.setCreateDate(new Date());
            project = projectService.persist(project);
            listProjectDTO.add(project);
        }

        for (int i = 0; i < tasksNum; i++) {
            TaskDTO task = new TaskDTO();
            task.setId(UUID.randomUUID().toString());
            task.setName("project" + i);
            task.setDescription("desc");
            task.setCreateDate(new Date());
            task.setProjectId(listProjectDTO.get(i/10).getId());
            task = taskService.persist(task);
            listTaskDTO.add(task);
        }

        projectService.delete(listProjectDTO.remove(0));

        int i = 0;
        Iterator<TaskDTO> iteratorTaskDTO = listTaskDTO.iterator();
        while (i < 10) {
            if (iteratorTaskDTO.hasNext()) {
                iteratorTaskDTO.next();
                iteratorTaskDTO.remove();
            }
            i++;
        }

        @NotNull final List<ProjectDTO> actualProjects = projectService.getAll();
        actualProjects.sort(Sort.NAME.getComparatorDTO());
        listProjectDTO.sort(Sort.NAME.getComparatorDTO());
        @NotNull final List<TaskDTO> actualTasks = taskService.getAll();
        actualTasks.sort(Sort.NAME.getComparatorDTO());
        listTaskDTO.sort(Sort.NAME.getComparatorDTO());

        Assert.assertEquals(listProjectDTO.toString(), actualProjects.toString());
        Assert.assertEquals(listTaskDTO.toString(), actualTasks.toString());
    }

    @Test
    public void projectByNameTest() {
        final int projectsNum = 20;

        @NotNull final List<ProjectDTO> listProjectDTO = new ArrayList<>();

        for (int i = 0; i < projectsNum; i++) {
            ProjectDTO project = new ProjectDTO();
            project.setId(UUID.randomUUID().toString());
            project.setName("project" + i);
            project.setDescription("desc");
            project.setCreateDate(new Date());
            project = projectService.persist(project);

            if (project.getName().contains("project1")) {
                listProjectDTO.add(project);
            }
        }

        @NotNull final List<ProjectDTO> actualProjects =
                projectService.findByName("project1");
        actualProjects.sort(Sort.NAME.getComparatorDTO());
        listProjectDTO.sort(Sort.NAME.getComparatorDTO());

        Assert.assertEquals(listProjectDTO.toString(), actualProjects.toString());
    }

    @Test
    public void  projectByDescriptionTest() {
        final int projectsNum = 20;

        @NotNull final List<ProjectDTO> listProjectDTO = new ArrayList<>();

        for (int i = 0; i < projectsNum; i++) {
            ProjectDTO project = new ProjectDTO();
            project.setId(UUID.randomUUID().toString());
            project.setName("project" + i);
            project.setDescription("desc" + i);
            project.setCreateDate(new Date());
            project = projectService.persist(project);

            if (project.getDescription().contains("desc1")) {
                listProjectDTO.add(project);
            }
        }

        @NotNull final List<ProjectDTO> actualProjects =
                projectService.findByDescription("desc1");
        actualProjects.sort(Sort.NAME.getComparatorDTO());
        listProjectDTO.sort(Sort.NAME.getComparatorDTO());

        Assert.assertEquals(listProjectDTO.toString(), actualProjects.toString());
    }

    @NotNull
    private String generateUserName() {
        @NotNull final Random random = new Random();
        return "TestServiceUser" + random.nextInt(999999);
    }

    @SneakyThrows
    private void initializeProjectService() {
        final URL projectUrl = new URL(ConstantApplication.PROJECT_ENDPOINT_URL + "?wsdl");
        final QName projectQname = new QName("http://endpoint.evseenko.ru/", "ProjectEndpointService");
        final Service projectServ = Service.create(projectUrl, projectQname);
        projectService = projectServ.getPort(IProjectEndpoint.class);
    }

    @SneakyThrows
    private void initializeTaskService() {
        final URL taskUrl = new URL(ConstantApplication.TASK_ENDPOINT_URL + "?wsdl");
        final QName taskQname = new QName("http://endpoint.evseenko.ru/", "TaskEndpointService");
        final Service taskServ = Service.create(taskUrl, taskQname);
        taskService = taskServ.getPort(ITaskEndpoint.class);
    }

    @SneakyThrows
    private void initializeAuthenticationService() {
        final URL authUrl = new URL(ConstantApplication.AUTHENTICATION_ENDPOINT_URL + "?wsdl");
        final QName authQname = new QName("http://endpoint.evseenko.ru/", "AuthenticationEndpointService");
        final Service authServ = Service.create(authUrl, authQname);
        authenticationService = authServ.getPort(IAuthenticationEndpoint.class);
    }

    @SneakyThrows
    private void initializeRegistrationService() {
        final URL registrationUrl = new URL(ConstantApplication.REGISTRATION_ENDPOINT_URL + "?wsdl");
        final QName registrationQname = new QName("http://endpoint.evseenko.ru/", "RegistrationEndpointService");
        final Service registrationServ = Service.create(registrationUrl, registrationQname);
        registrationService = registrationServ.getPort(IRegistrationEndpoint.class);
    }

    private void authenticate() {
        ((BindingProvider)authenticationService).getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        authenticationService.authenticate(testUserDTO.getLogin(), "12345678");
        final Map<String, Object> reqCtx = ((BindingProvider)authenticationService).getResponseContext();
        final Map<String, List<String>> response_headers = (Map<String, List<String>>) reqCtx.get(MessageContext.HTTP_RESPONSE_HEADERS);

        ((BindingProvider)projectService).getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        final Map<String, List<String>> projectServiceHeaders = new HashMap<>();
        projectServiceHeaders.put("Cookie", response_headers.get("Set-Cookie"));
        ((BindingProvider)projectService).getRequestContext().put(
                MessageContext.HTTP_REQUEST_HEADERS,
                projectServiceHeaders
        );

        ((BindingProvider)taskService).getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        final Map<String, List<String>> taskServiceHeaders = new HashMap<>();
        taskServiceHeaders.put("Cookie", response_headers.get("Set-Cookie"));
        ((BindingProvider)taskService).getRequestContext().put(
                MessageContext.HTTP_REQUEST_HEADERS,
                taskServiceHeaders
        );
    }
}
