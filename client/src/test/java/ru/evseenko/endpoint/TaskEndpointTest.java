package ru.evseenko.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.evseenko.api.endpoint.IAuthenticationEndpoint;
import ru.evseenko.api.endpoint.IProjectEndpoint;
import ru.evseenko.api.endpoint.IRegistrationEndpoint;
import ru.evseenko.api.endpoint.ITaskEndpoint;
import ru.evseenko.entity.dto.ProjectDTO;
import ru.evseenko.entity.dto.TaskDTO;
import ru.evseenko.entity.dto.UserDTO;
import ru.evseenko.util.ConstantApplication;
import ru.evseenko.util.Sort;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public class TaskEndpointTest {

    private IProjectEndpoint projectService;

    private ITaskEndpoint taskService;

    private IAuthenticationEndpoint authenticationService;

    private IRegistrationEndpoint registrationService;

    private UserDTO testUserDTO;

    @Before
    public void init() {
        initializeAuthenticationService();
        initializeRegistrationService();
        initializeProjectService();
        initializeTaskService();

        final UserDTO testUser = new UserDTO();
        testUser.setLogin(generateUserName());
        testUser.setPasswordHash("12345678");
        testUserDTO = registrationService.register(testUser);
        testUserDTO.setPasswordHash("12345678");

        authenticate();
    }

    @After
    public void tearDown() {
        registrationService.remove(testUserDTO);
    }

    @Test
    public void taskCreateTest() {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("task1");
        task.setDescription("desc");
        task.setStartDate(new Date());
        task.setEndDate(new Date());

        task = taskService.persist(task);

        @NotNull final TaskDTO actualTask =
                taskService.get(task.getId());

        Assert.assertEquals(task.toString(), actualTask.toString());
    }

    @Test
    public void projectUpdateTest() {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("Task 1");
        task.setDescription("desc");
        task.setStartDate(new Date());
        task.setEndDate(new Date());

        task = taskService.persist(task);

        task.setName("upd_Task1");
        task.setDescription("upd_desc");
        task.setCreateDate(new Date());
        task.setStartDate(new Date());
        task.setEndDate(new Date());

        taskService.update(task);

        @NotNull final TaskDTO actualTask =
                taskService.get(task.getId());

        Assert.assertEquals(task.toString(), actualTask.toString());
    }

    @Test
    public void taskDeleteAllTest() {
        final int tasksNum = 20;

        for (int i = 0; i < tasksNum; i++) {
            final TaskDTO task = new TaskDTO();
            task.setName("task" + i);
            task.setDescription("desc");
            taskService.persist(task);
        }

        taskService.deleteAll();

        Assert.assertEquals(new ArrayList<>(), taskService.getAll());
    }

    @Test
    public void taskForProjectTest() {
        final int tasksNum = 200;

        @NotNull final List<TaskDTO> listTaskDTO = new ArrayList<>();
        ProjectDTO project = new ProjectDTO();

        project.setName("project");
        project.setDescription("desc");
        project.setCreateDate(new Date());
        project = projectService.persist(project);

        for (int i = 0; i < tasksNum; i++) {
            TaskDTO task = new TaskDTO();
            task.setName("project" + i);
            task.setDescription("desc");
            task.setProjectId(((i % 10) > 5) ? project.getId() : "");
            task = taskService.persist(task);

            if (!task.getProjectId().isEmpty()) {
                listTaskDTO.add(task);
            }
        }

        @NotNull final List<TaskDTO> actualTasks = taskService.getTaskForProject(project.getId());
        actualTasks.sort(Sort.NAME.getComparatorDTO());
        listTaskDTO.sort(Sort.NAME.getComparatorDTO());

        Assert.assertEquals(listTaskDTO.toString(), actualTasks.toString());
    }

    @Test
    public void taskByNameTest() {
        final int tasksNum = 20;

        @NotNull final List<TaskDTO> listTaskDTO = new ArrayList<>();

        for (int i = 0; i < tasksNum; i++) {
            TaskDTO task = new TaskDTO();
            task.setId(UUID.randomUUID().toString());
            task.setName("task" + i);
            task.setDescription("desc");
            task = taskService.persist(task);

            if (task.getName().contains("task1")) {
                listTaskDTO.add(task);
            }
        }

        @NotNull final List<TaskDTO> actualTasks =
                taskService.findByName("task1");
        actualTasks.sort(Sort.NAME.getComparatorDTO());
        listTaskDTO.sort(Sort.NAME.getComparatorDTO());

        Assert.assertEquals(listTaskDTO.toString(), actualTasks.toString());
    }

    @Test
    public void taskByDescriptionTest() {
        final int tasksNum = 20;

        @NotNull final List<TaskDTO> listTaskDTO = new ArrayList<>();

        for (int i = 0; i < tasksNum; i++) {
            TaskDTO task = new TaskDTO();
            task.setName("task" + i);
            task.setDescription("desc" + i);
            task = taskService.persist(task);

            if (task.getDescription().contains("desc1")) {
                listTaskDTO.add(task);
            }
        }

        @NotNull final List<TaskDTO> actualTasks =
                taskService.findByDescription("desc1");
        actualTasks.sort(Sort.NAME.getComparatorDTO());
        listTaskDTO.sort(Sort.NAME.getComparatorDTO());

        Assert.assertEquals(listTaskDTO.toString(), actualTasks.toString());
    }

    @NotNull
    private String generateUserName() {
        @NotNull final Random random = new Random();
        return "TestServiceUser" + random.nextInt(999999);
    }

    @SneakyThrows
    private void initializeProjectService() {
        final URL projectUrl = new URL(ConstantApplication.PROJECT_ENDPOINT_URL + "?wsdl");
        final QName projectQname = new QName("http://endpoint.evseenko.ru/", "ProjectEndpointService");
        final Service projectServ = Service.create(projectUrl, projectQname);
        projectService = projectServ.getPort(IProjectEndpoint.class);
    }

    @SneakyThrows
    private void initializeTaskService() {
        final URL taskUrl = new URL(ConstantApplication.TASK_ENDPOINT_URL + "?wsdl");
        final QName taskQname = new QName("http://endpoint.evseenko.ru/", "TaskEndpointService");
        final Service taskServ = Service.create(taskUrl, taskQname);
        taskService = taskServ.getPort(ITaskEndpoint.class);
    }

    @SneakyThrows
    private void initializeAuthenticationService() {
        final URL authUrl = new URL(ConstantApplication.AUTHENTICATION_ENDPOINT_URL + "?wsdl");
        final QName authQname = new QName("http://endpoint.evseenko.ru/", "AuthenticationEndpointService");
        final Service authServ = Service.create(authUrl, authQname);
        authenticationService = authServ.getPort(IAuthenticationEndpoint.class);
    }

    @SneakyThrows
    private void initializeRegistrationService() {
        final URL registrationUrl = new URL(ConstantApplication.REGISTRATION_ENDPOINT_URL + "?wsdl");
        final QName registrationQname = new QName("http://endpoint.evseenko.ru/", "RegistrationEndpointService");
        final Service registrationServ = Service.create(registrationUrl, registrationQname);
        registrationService = registrationServ.getPort(IRegistrationEndpoint.class);
    }

    private void authenticate() {
        ((BindingProvider)authenticationService).getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        authenticationService.authenticate(testUserDTO.getLogin(), "12345678");
        final Map<String, Object> reqCtx = ((BindingProvider)authenticationService).getResponseContext();
        final Map<String, List<String>> response_headers = (Map<String, List<String>>) reqCtx.get(MessageContext.HTTP_RESPONSE_HEADERS);

        ((BindingProvider)projectService).getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        final Map<String, List<String>> projectServiceHeaders = new HashMap<>();
        projectServiceHeaders.put("Cookie", response_headers.get("Set-Cookie"));
        ((BindingProvider)projectService).getRequestContext().put(
                MessageContext.HTTP_REQUEST_HEADERS,
                projectServiceHeaders
        );

        ((BindingProvider)taskService).getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        final Map<String, List<String>> taskServiceHeaders = new HashMap<>();
        taskServiceHeaders.put("Cookie", response_headers.get("Set-Cookie"));
        ((BindingProvider)taskService).getRequestContext().put(
                MessageContext.HTTP_REQUEST_HEADERS,
                taskServiceHeaders
        );
    }
}
