package ru.evseenko.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.evseenko.api.endpoint.IAuthenticationEndpoint;
import ru.evseenko.api.endpoint.IRegistrationEndpoint;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.dto.UserDTO;
import ru.evseenko.util.ConstantApplication;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

public class UserEndpointTest {

    private IAuthenticationEndpoint authenticationService;

    private IRegistrationEndpoint registrationService;

    private UserDTO user;

    @Before
    public void setUpTest() {
        initializeRegistrationService();
        initializeAuthenticationService();

        user = new UserDTO();
        user.setLogin("LOGIN");
        user.setPasswordHash("password hash");
        user.setRole(Role.ADMIN);
        user = registrationService.register(user);
    }

    @After
    public void tearDownTest() {
        registrationService.remove(user);
    }

    @Test
    public void createUserTest() {
        @NotNull final UserDTO actualUser =
                authenticationService.authenticate(user.getLogin(), "password hash");
        Assert.assertEquals(user.toString(), actualUser.toString());
    }

    @SneakyThrows
    private void initializeAuthenticationService() {
        final URL authUrl = new URL(ConstantApplication.AUTHENTICATION_ENDPOINT_URL + "?wsdl");
        final QName authQname = new QName("http://endpoint.evseenko.ru/", "AuthenticationEndpointService");
        final Service authServ = Service.create(authUrl, authQname);
        authenticationService = authServ.getPort(IAuthenticationEndpoint.class);
    }

    @SneakyThrows
    private void initializeRegistrationService() {
        final URL registrationUrl = new URL(ConstantApplication.REGISTRATION_ENDPOINT_URL + "?wsdl");
        final QName registrationQname = new QName("http://endpoint.evseenko.ru/", "RegistrationEndpointService");
        final Service registrationServ = Service.create(registrationUrl, registrationQname);
        registrationService = registrationServ.getPort(IRegistrationEndpoint.class);
    }
}
